$(document).ready(function(){


    var owlHeader = $('.header-slides');
    owlHeader.owlCarousel({
      items: 1,
      dots: true,
      dotsContainer: '#dots',
      nav: true,
      navContainer : '.controller-top',
      navText : ['<div class="owl-prev">Назад</div>', '<div class="owl-next">Вперед</div>'],
      onInitialized: callback
    });

    
    function callback(event) {
      var NumberHeader = event.item.index + 1;
      $('.controller-number .current').text('0'+NumberHeader)

      var NumberHeaderAll = event.item.count;
      $('.controller-number .all').text('0'+NumberHeaderAll)
    }
    // Listen to owl events:
    owlHeader.on('changed.owl.carousel', function(event) {
      var NumberHeader = event.item.index + 1;
      $('.controller-number .current').text('0'+NumberHeader)
    })

    $("ul.mn-menu, ul.cnt-ul").on("click","a", function (event) {
      //отменяем стандартную обработку нажатия по ссылке
      event.preventDefault();
  
      //забираем идентификатор бока с атрибута href
      var id  = $(this).attr('href'),
  
      //узнаем высоту от начала страницы до блока на который ссылается якорь
        top = $(id).offset().top;
      
      //анимируем переход на расстояние - top за 1500 мс
      $('body,html').animate({scrollTop: top}, 1500);
    });


    $('.header-burger').on('click',function(){
      $(this).prev().children().slideDown()
      $(this).prev().children().css('z-index','35')
      $('#dots').css('z-index','-1')
    })
    
    $('.header .close, .footer .close').on('click',function(){
      $(this).parent().slideUp();
      $('#dots').css('z-index','35')
    })

    if(window.innerWidth < 481){
      $('.mn-menu a').on('click',function(){
        $('.mn-menu').slideUp()
        $('#dots').css('z-index','35')
        
      })
  }

})